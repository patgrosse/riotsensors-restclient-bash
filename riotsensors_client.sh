#!/bin/bash
set -o pipefail
set -e

RS_HOST=${RS_HOST:-localhost}
RS_PORT=${RS_PORT:-9080}

JQ_EXPR_CALL='.result'
JQ_EXPR_LIST='.lambdas | map([.id, .name, .type.code]) | .[] | "\(.[0])\t\(.[1])\t\(.[2])"'

function die {
    echo >&2 "$@"
    exit 1
}

function usage_call {
	echo "Usage for lambda calls:"
	echo "$0 call name <name> <type>"
	echo "$0 call id <id> <type>"
	echo "Output format:"
	echo "Unquoted string, integer or floating point value"
}

function usage_list {
	echo "Usage for lambda lists:"
	echo "$0 list [<type>]"
	echo "Output format:"
	echo -e "<id>\t<name>\t<type>"
}

function usage {
	echo "riotsensors REST server Bash client v1.0"
	echo "by Patrick Grosse <patrick.grosse@uni-muenster.de>"
	echo
	usage_call
	echo
	usage_list
}


if [ "$#" -eq 0 ]; then
	usage
else
	if [ "$1" == "call" ]; then
		if [ $# -eq 4 ]; then
			if [ "$2" == "name" ]; then
				echo "$4" | grep -E -q '^[0-9]+$' || die "Numeric type required, $4 provided"
				curl -fsS "http://${RS_HOST}:${RS_PORT}/v1/call/name/${4}/${3}" | jq -r "${JQ_EXPR_CALL}"
			elif [ "$2" == "id" ]; then
				echo "$3" | grep -E -q '^[0-9]+$' || die "Numeric id required, $3 provided"
				echo "$4" | grep -E -q '^[0-9]+$' || die "Numeric type required, $4 provided"
				curl -fsS "http://${RS_HOST}:${RS_PORT}/v1/call/id/${4}/${3}" | jq -r "${JQ_EXPR_CALL}"
			else
				usage_call
				exit 1
			fi
		else
			usage_call
			exit 1
		fi
	elif [ "$1" == "list" ]; then
		if [ "$#" -eq 1 ]; then
			curl -fsS "http://${RS_HOST}:${RS_PORT}/v1/list" | jq -r "${JQ_EXPR_LIST}"
		elif [ "$#" -eq 2 ]; then
				echo "$2" | grep -E -q '^[0-9]+$' || die "Numeric type required, $2 provided"
			curl -fsS "http://${RS_HOST}:${RS_PORT}/v1/list?type=$2" | jq -r "${JQ_EXPR_LIST}"
		else
			usage_list
			exit 1
		fi
	else
		echo "Unknown operation $1"
		usage
		exit 1
	fi
fi
