# riotsensors-restclient-bash

A client for the riotsensors REST server written as a Bash script

## Dependencies

* curl
* jq

## Install

```bash
sudo make install
```

This will install to `/bin/riotsensors_client`

## Usage



```
riotsensors REST server Bash client v1.0
by Patrick Grosse <patrick.grosse@uni-muenster.de>

Usage for lambda calls:
riotsensors_client call name <name> <type>
riotsensors_client call id <id> <type>
Output format:
Unquoted string, integer or floating point value

Usage for lambda lists:
riotsensors_client list [<type>]
Output format:
<id>    <name>  <type>
```

